const express = require("express");
const app = express();
const port = 8000;

const multer = require('multer');
const path = require('path');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
  }
})

const upload = multer({storage: storage});


app.use(express.json());

const users = [];
users.push(
  {
    "nama": "Hello World",
    "alamat": "Jl. XYZ",
    "npm": "123123",
  }
)

app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});

app.get("/mahasiswa/all", (req, res) => {
  res.json(users);
});

app.get("/mahasiswa/:npm", (req, res) => {
  var user = users.filter((_user) => {return _user.npm === req.params.npm});
  res.json(user[0]);
});

app.post("/mahasiswa", (req, res) => {
  var user = users.filter((_user) => {return _user.npm === req.body["npm"]})[0];

  if (user === undefined) {
    var new_user = {
      "nama": req.body["nama"],
      "alamat": req.body["alamat"],
      "npm": req.body["npm"],
    }
    users.push(new_user)
  }
  res.json("Request sent")
});

app.put("/mahasiswa/:npm", (req, res) => {
  var user = users.filter((_user) => {return _user.npm === req.params.npm})[0];
  var index = users.findIndex((_user) => {return _user.npm === req.params.npm});

  if (user !== undefined && index > -1) {
    user["nama"] = req.body["nama"]
    user["alamat"] = req.body["alamat"]

    users[index] = user
  }
  res.json("Request sent")
})

app.delete("/mahasiswa/:npm", (req, res) => {
  var index = users.findIndex((_user) => {return _user.npm === req.params.npm});
  if (index > -1) {
    users.splice(index, 1)
  }
  res.json("Request sent")
})

app.post("/file", upload.single('uploaded_file'), (req, res, next) => {
  const options = {
    root: path.join(__dirname, 'uploads'),
    dotfiles: 'deny',
    headers: {
      'x-timestamp': Date.now(),
      'x-sent': true
    }
  }

  const fileName = req.file.filename
  res.sendFile(fileName, options, (err) => {
    if (err) {
      next(err)
    } else {
      console.log('Sent:', fileName)
    }
  })
})
